import { IContact } from '../contact/contact';

export interface IGroup {
  members: Array<IContact>;
  Id: string;
}
