import { Injectable } from '@angular/core';
import { IGroup } from './group';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GroupService {
  private url = 'http://18.221.19.138:3000';

  constructor(private http: HttpClient) {}
  addGroup() {
    return this.http.post(`${this.url}/groupchat`, {
      nickName: localStorage.getItem('nickName'),
      email: localStorage.getItem('email')
    });
  }

  getGroups(): Observable<Array<IGroup>> {
    return this.http.get<Array<IGroup>>(`${this.url}/groupchat`);
  }

  addMember(chatRoomId: string, nickName: string, email: string) {
    return this.http.post(`${this.url}/groupchat/addMember`, {
      chatRoomId: chatRoomId,
      nickName: nickName,
      email: email
    });
  }
}
