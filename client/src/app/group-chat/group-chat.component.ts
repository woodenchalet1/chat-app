import { Component, OnInit } from '@angular/core';
import { IGroup } from './group';
import { GroupService } from './group.service';
import { ContactService } from '../contact/contact.service';
import { IContact } from '../contact/contact';
import { ChatService } from '../chat-room/chat.service';

@Component({
  selector: 'app-group-chat',
  templateUrl: './group-chat.component.html',
  styleUrls: ['./group-chat.component.css']
})
export class GroupChatComponent implements OnInit {
  groups: IGroup[];
  contacts: IContact[];

  constructor(
    private _groupService: GroupService,
    private _contactService: ContactService,
    private _chatService: ChatService
  ) {}

  addMember(chatRoomId: string, nickName: string, email: string) {
    this._groupService
      .addMember(chatRoomId, nickName, email)
      .subscribe(data => {

        const group = this.groups.find(ele => {
          return ele.Id === chatRoomId;
        });

        group.members.push({ nickName: nickName, email: email });
      });
  }
  getGroups() {
    this._groupService.getGroups().subscribe(data => {
      this.groups = data;
      this.groups = this.groups.filter(group => {
        const result = group.members.map(({ email }) => email);
        return (result.includes(localStorage.getItem('email')));
      });
      this._chatService.roomID.next(this.groups[0].Id);
    });
  }

  startGroupChat(chatRoomId: string) {
    this._chatService.roomID.next(chatRoomId);
  }

  getContacts() {
    this._contactService.getContacts().subscribe(data => {
      this.contacts = data;
    });
  }

  addGroup() {
    this._groupService.addGroup().subscribe(() => {
      this.getGroups();
    });
  }

  ngOnInit() {
    this.getGroups();
    this.getContacts();
  }
}
