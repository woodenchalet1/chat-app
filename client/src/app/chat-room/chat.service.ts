import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable, BehaviorSubject } from 'rxjs';
import { IMessage } from './message';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private url = 'http://18.221.19.138:3000';
  private socket;
  public roomID: BehaviorSubject<string> = new BehaviorSubject('');
  constructor(private http: HttpClient) {
    this.socket = io(this.url);
  }

  sendMessage(message: IMessage) {
    this.socket.emit('add-message', message);
  }

  getHistory(chatroomId: string): Observable<Array<IMessage>> {
    return this.http.post<Array<IMessage>>(`${this.url}/groupchat/history`, {
      chatRoomId: chatroomId
    });
  }

  getMessages(): Observable<IMessage> {
    const message = new Observable<IMessage>(observer => {
      this.socket.on('message', data => {
        console.log(data);
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return message;
  }
}
