import { Component, OnInit } from '@angular/core';
import { ChatService } from './chat.service';
import { IMessage } from './message';

@Component({
  selector: 'app-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.css']
})
export class ChatRoomComponent implements OnInit {
  message: string;
  history = Array<IMessage>();
  constructor(private chatService: ChatService) {}

  ngOnInit() {
    this.getHistory();
    this.getMessages();
  }

  getHistory() {
    this.chatService.roomID.subscribe(data => {
      if (data !== '') {
        this.chatService.getHistory(data).subscribe(messages => {
          console.log(messages);
          this.history = messages;
        });
      }
    });
  }

  getMessages() {
    this.chatService.getMessages().subscribe(data => {
      this.chatService
        .getHistory(this.chatService.roomID.value)
        .subscribe(messages => {
          this.history = messages;
        });
    });
  }

  senderMessage(message: IMessage) {
    return message.sender.email === localStorage.getItem('email');
  }

  send() {
    this.chatService.sendMessage({
      content: this.message,
      sender: {
        nickName: localStorage.getItem('nickName'),
        email: localStorage.getItem('email')
      },
      roomId: this.chatService.roomID.value
    });
  }
}
