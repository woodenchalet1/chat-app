import { User } from './user';

export class IMessage {
  content: string;
  Id?: string;
  sender: User;
  dataTime?: Date;
  roomId?: string;
}
