import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = 'http://18.221.19.138:3000';

  constructor(private http: HttpClient) {}

  signup(userInfo: User) {
    console.log(userInfo);
    // tslint:disable-next-line:no-debugger
    return this.http.post(`${this.url}/users`, {
      nickName: userInfo.nickName,
      password: userInfo.password,
      email: userInfo.email
    });
  }

  logIn(userInfo: User) {
    return this.http
      .post<{ token: string; nickName: string; email: string }>(
        `${this.url}/users/login`,
        {
          email: userInfo.email,
          password: userInfo.password
        }
      )
      .subscribe(response => {
        const token = response.token;
        const email = response.email;
        const nickName = response.nickName;
        console.log(token);
        console.log(email);
        console.log(nickName);

        if (token) {
          this.saveAuthData(token, email, nickName);
        }
      });
  }

  private saveAuthData(token: string, email: string, nickName: string) {
    localStorage.setItem('token', token);
    localStorage.setItem('email', email);
    localStorage.setItem('nickName', nickName);
  }

  public getAuthData() {
    return {
      token: localStorage.getItem('token'),
      email: localStorage.getItem('email'),
      nickName: localStorage.getItem('nickName')
    };
  }
}
