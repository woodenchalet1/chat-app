import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { User } from './user';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  user: User;
  constructor(private userService: UserService) {
    this.user = new User();
  }

  ngOnInit() {}

  login() {
    this.userService.logIn(this.user);
  }
  signUp() {
    this.userService.signup(this.user).subscribe();
  }
}
