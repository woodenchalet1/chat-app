import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {ContactComponent} from './contact/contact.component';
import { ReceivedMessageComponent } from './received-message/received-message.component';
import { SentMessageComponent } from './sent-message/sent-message.component';
import { GroupChatComponent } from './group-chat/group-chat.component';
import { ChatRoomComponent } from './chat-room/chat-room.component';
import { ChatAppComponent } from './chat-app/chat-app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { AuthInterceptor } from './auth/auth-interceptor';

@NgModule({
  declarations: [
    AppComponent,
      ContactComponent,
      ReceivedMessageComponent,
      SentMessageComponent,
      GroupChatComponent,
      ChatRoomComponent,
      ChatAppComponent,
      NavBarComponent
  ],
  imports: [
    FontAwesomeModule,
      BrowserModule,
      FormsModule,
      HttpClientModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

