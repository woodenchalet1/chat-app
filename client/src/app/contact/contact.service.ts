import { Injectable } from '@angular/core';
import { IContact } from './contact';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private url = 'http://18.221.19.138:3000';

  constructor(private http: HttpClient) {}

  getContacts(): Observable<Array<IContact>> {
    return this.http.get<Array<IContact>>(`${this.url}/users`);
  }
}
