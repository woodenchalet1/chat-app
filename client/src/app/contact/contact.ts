export interface IContact {
    nickName: string;
    email: string;
    id?: string;
}
