import { Component, OnInit } from '@angular/core';
import { IContact } from './contact';
import { ContactService } from './contact.service';
import { GroupService } from '../group-chat/group.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contacts: IContact[];

  constructor(private _contactService: ContactService,
    private _groupService: GroupService) {}

  getContacts() {
    this._contactService.getContacts().subscribe(data => {
      this.contacts = data;

      console.log(this.contacts);
    });
  }

  ngOnInit(): void {
    console.log(this.contacts);
    this.getContacts();
  }
}
