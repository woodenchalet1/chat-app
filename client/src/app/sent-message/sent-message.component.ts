import { Component, Input } from '@angular/core';
import { IMessage } from '../chat-room/message';

@Component({
  selector: 'app-sent-message',
  templateUrl: './sent-message.component.html',
  styleUrls: ['./sent-message.component.css']
})
export class SentMessageComponent {
  @Input() message: IMessage;

}
