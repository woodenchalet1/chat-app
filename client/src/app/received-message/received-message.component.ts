import { Component, Input } from '@angular/core';
import { IMessage } from '../chat-room/message';

@Component({
  selector: 'app-received-message',
  templateUrl: './received-message.component.html',
  styleUrls: ['./received-message.component.css']
})
export class ReceivedMessageComponent {
  @Input() message: IMessage;
}
