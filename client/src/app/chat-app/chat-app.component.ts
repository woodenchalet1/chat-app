import {Component} from '@angular/core';
import {faCog, faPaperclip, faCommentAlt, faUser, faSearch} from '@fortawesome/free-solid-svg-icons';
import { ContactService } from '../contact/contact.service';

@Component({
    selector: 'app-chat-app',
    templateUrl: './chat-app.component.html',
    styleUrls: ['./chat-app.component.css'],
    providers: [ContactService]
})
export class ChatAppComponent {
    faSetting = faCog;
    faPaperClip = faPaperclip;
    faCommentDots = faCommentAlt;
    faUser = faUser;
    faSearch = faSearch;
}
