import * as express from 'express';
import { Container } from 'inversify';
import { container } from '../inversify.config';
import { UserSignUpRepository } from '../infrastructure/repositories/user-sign-up.repository';
import TYPES from '../constant/types';
import { verify } from 'jsonwebtoken';

function authMiddlewareFactory(container: Container) {
    return ( role?: string ) => {
        return (
            req: express.Request,
            res: express.Response,
            next: express.NextFunction
        ) => {
            const userRepository = container.get<UserSignUpRepository>(
                TYPES.UserSignUpRepository
            );

            (async () => {
                // get email using auth token
                const token = req.headers.authorization.split(" ")[1];
                const decodedToken: any = verify(token, "secret");

                let email = decodedToken.email;
                if (email !== null) {
                    // find user with matching email
                    const matched = await userRepository.getUser({
                        Email: email
                    });

                    // Check user has required role
                    if (
                        matched
                    ) {
                        next();
                    } else {
                        res.status(403).end('Forbidden');
                    }
                } else {
                    res.status(401).end('Unauthorized');
                }
            })();
        };
    };
}

const authMiddleware = authMiddlewareFactory(container);

export { authMiddleware };
