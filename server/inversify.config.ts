import { Container } from 'inversify';
import { UserHandler } from './handlers/user-sign-up.handler';
import TYPES from './constant/types';
import {
    UserSignUpRepository,
    UserSignUpDataRepository
} from './infrastructure/repositories/user-sign-up.repository';
import { CreateGroupChatHandler } from './handlers/create-group-chat-handler';
import { GroupChatDataRepository, GroupChatRepository } from './infrastructure/repositories/group-chat.repository';

let container = new Container();

container.bind<UserHandler>(TYPES.UserSignUpHandler).to(UserHandler);
container
    .bind<UserSignUpRepository>(TYPES.UserSignUpRepository)
    .to(UserSignUpRepository);

container
    .bind<UserSignUpDataRepository>(TYPES.UserSignUpDataRepository)
    .to(UserSignUpDataRepository);

container
    .bind<CreateGroupChatHandler>(TYPES.CreateGroupChatHandler)
    .to(CreateGroupChatHandler);

container
    .bind<GroupChatDataRepository>(TYPES.GroupChatDataRepository)
    .to(GroupChatDataRepository);

container
    .bind<GroupChatRepository>(TYPES.GroupChatRepository)
    .to(GroupChatRepository);

export { container };
