import 'reflect-metadata';
import { InversifyExpressServer } from 'inversify-express-utils';
import { makeLoggerMiddleware } from 'inversify-logger-middleware';
import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';
import * as YAML from 'yamljs';
import * as socketIo from 'socket.io';
import * as cors from 'cors';
import * as swaggerUi from 'swagger-ui-express';
import * as mongoose from 'mongoose';
import * as methodOverride from 'method-override';
import './controllers/user.controller';
import './controllers/group-chat.controller';
import { container } from './inversify.config';
import TYPES from './constant/types';
import { GroupChatRepository } from './infrastructure/repositories/group-chat.repository';
import { CreateGroupChatHandler } from './handlers/create-group-chat-handler';

// load the api open definition.
const swaggerDocument = YAML.load('../docs/api.yaml');

// load everything needed to the Container

if (process.env.NODE_ENV === 'development') {
    let logger = makeLoggerMiddleware();
    container.applyMiddleware(logger);
}

// start the server
let server = new InversifyExpressServer(container);
server.setConfig(app => {
    app.use('/api', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    app.use(
        bodyParser.urlencoded({
            extended: true
        })
    );
    app.use(methodOverride('X-HTTP-Method-Override'));
    app.use(cors({ credentials: true, origin: true }));
    app.use(bodyParser.json());
    app.use(helmet());
});

mongoose.connect('mongodb://root:example@localhost:27017/admin', {
    useNewUrlParser: true
});

let app = server.build();
let instance = app.listen(3000);

let io = socketIo.listen(instance);

io.on('connection', function(socket) {
    socket.on('disconnect', function() {
        console.log('disconnected');
    });

    socket.on('add-message', data => {
        //send message to user in this room
        const chatHandler = container.get<CreateGroupChatHandler>(
            TYPES.CreateGroupChatHandler
        );
        console.log(data);
        chatHandler.HandleAddChatToGroup(
            data.sender.nickName,
            data.sender.email,
            data.content,
            data.roomId
        );
        io.emit('message', data);
    });
});

console.log('Server started on port 3000 :)');

exports = module.exports = app;
