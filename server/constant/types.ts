const TYPES = {
    UserSignUpRepository: Symbol.for('IUserSignUpRepository'),
    UserSignUpHandler: Symbol.for('IUserSignUpHandler'),
    UserSignUpDataRepository: Symbol.for('UserSignUpDataRepository'),
    GroupChatRepository: Symbol.for('GroupChatRepository'),
    CreateGroupChatHandler: Symbol.for('CreateGroupChatHandler'),
    GroupChatDataRepository: Symbol.for('GroupChatDataRepository'),
};

export default TYPES;