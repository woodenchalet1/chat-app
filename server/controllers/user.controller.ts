import { controller, httpPost, httpGet } from 'inversify-express-utils';
import { Request, Response } from 'express';
import { inject } from 'inversify';
import { UserHandler } from '../handlers/user-sign-up.handler';
import TYPES from '../constant/types';
import { authMiddleware } from '../middleware/auth_middleware';

@controller('/users')
export class UserController {
    constructor(
        @inject(TYPES.UserSignUpHandler) private handler: UserHandler
    ) {}

    @httpPost('/')
    async create(request: Request, response: Response) {
        try {
            let result = await this.handler.HandleSignUp(
                request.body.nickName,
                request.body.email,
                request.body.password
            );
            response.status(201).json(result);;
        } catch (error) {
            response.status(400).json({ error: error.message });
        }
    }

    @httpPost('/login')
    async login(request: Request, response: Response) {
        try {
            let result = await this.handler.HandleLogin(
                request.body.email,
                request.body.password
            );
            response.status(201).json(result);;
        } catch (error) {
            response.status(401).json({ error: "Auth failed" });
        }
    }

    @httpGet('/', authMiddleware())
    async get(request: Request, response: Response) {
        try {
            let result = await this.handler.HandleGetAllUser();
            response.status(201).json(result);;
        } catch (error) {
            response.status(400).json({ error: error.message });
        }
    }
}
