import { controller, httpPost, httpGet } from 'inversify-express-utils';
import { inject } from 'inversify';
import { Request, Response } from 'express';
import TYPES from '../constant/types';
import { CreateGroupChatHandler } from '../handlers/create-group-chat-handler';
import { authMiddleware } from '../middleware/auth_middleware';
import { GroupChatRoom } from '../domain/aggregates/user-start-group-chat.aggregate/group-chat-room';

@controller('/groupchat')
export class GroupChatController {
    constructor(
        @inject(TYPES.CreateGroupChatHandler)
        private handler: CreateGroupChatHandler
    ) {}

    @httpPost('/', authMiddleware())
    async create(request: Request, response: Response) {
        try {
            let result = await this.handler.HandleCreateGroupChat(
                request.body.nickName,
                request.body.email
            );
            response.status(201).json(result);
        } catch (error) {
            response.status(400).json({ error: error.message });
        }
    }

    @httpPost('/addMember', authMiddleware())
    async addMember(request: Request, response: Response) {
        try {
            let result = await this.handler.HandleAddMember(
                request.body.chatRoomId,
                request.body.nickName,
                request.body.email
            );
            response.status(201).json(result);
        } catch (error) {
            response.status(400).json({ error: error.message });
        }
    }

    @httpPost('/history', authMiddleware())
    async getHistory(request: Request, response: Response) {

        try {
            let result = await this.handler.HandleGetGroupChat(
                request.body.chatRoomId
            );
            response.status(201).json(result);
        } catch (error) {
            response.status(400).json({ error: error.message });
        }
    }

    @httpGet('/', authMiddleware())
    async get(request: Request, response: Response) {
        try {
            let result = await this.handler.HandleGetGroups();
            response.status(201).json(result);
        } catch (error) {
            response.status(400).json({ error: error.message });
        }
    }
}
