import { User } from '../../domain/aggregates/sign-up.aggregate/user';
import { IUserSignUpRepository } from '../../domain/aggregates/sign-up.aggregate/user-sign-up.repository.interface';
import { injectable, inject } from 'inversify';
import { UserDocument } from '../data-access/documents/user.document';
import TYPES from '../../constant/types';
import { MongoRepository } from './mongo.repository';
import { UserModel } from '../data-access/models/user.model';

@injectable()
export class UserSignUpRepository implements IUserSignUpRepository {
    constructor(
        @inject(TYPES.UserSignUpDataRepository)
        private UserSignUpDataRepository: UserSignUpDataRepository
    ) {}
    public async UserSignUp(
        nickName: string,
        email: string,
        password: string
    ): Promise<User> {
        let user: UserDocument = {
            NickName: nickName,
            Email: email,
            Password: password
        };
        let result: UserDocument = await this.UserSignUpDataRepository.create(
            user
        );
        return new User(
            result._id,
            result.NickName,
            result.Email,
            result.Password
        );
    }
    public async getUsers(): Promise<Array<User>> {
        let users: Array<User> = new Array<User>();
        let userDocuments: Array<
            UserDocument
        > = await this.UserSignUpDataRepository.retrieve();

        userDocuments.forEach(user => {
            users.push(new User(user._id, user.NickName, user.Email));
        });

        return users;
    }

    public async getUser(conditions): Promise<User> {
        let userDocument: UserDocument = await this.UserSignUpDataRepository.getOne(
            conditions
        );
        return new User(userDocument._id, userDocument.NickName, userDocument.Email);
    }
}

@injectable()
export class UserSignUpDataRepository extends MongoRepository<UserDocument> {
    constructor() {
        super(UserModel);
    }

    async getOne(conditions): Promise<UserDocument> {
        return await UserModel.findOne(conditions).exec();
    }
}
