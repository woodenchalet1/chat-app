import { Document, Model, Types } from 'mongoose';
import { unmanaged, injectable } from 'inversify';
import { IMongoRepository } from './interfaces/mongo.repository.interface';

@injectable()
export class MongoRepository<T extends Document> implements IMongoRepository<T>{
  protected _model: Model<Document>;

  constructor(@unmanaged() model: Model<Document>) {
    this._model = model;
  }

  async create(item: T): Promise<T> {
    return await this._model.create(item);
  }

  async retrieve(): Promise<Array<T>> {
    return await this._model.find().exec();
  }

  async update(_id: Types.ObjectId, item: T) {
    return await this._model.update({ _id: _id }, item);
  }

  async delete(_id: string): Promise<T> {
    return await this._model.remove({ _id: this.toObjectId(_id) });
  }

  async findById(_id: string): Promise<T> {
    return await this._model.findById(_id);
  }

  private toObjectId(_id: string): Types.ObjectId {
    return Types.ObjectId.createFromHexString(_id);
  }
}
