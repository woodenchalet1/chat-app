import { Document, Types } from "mongoose";

export interface IMongoRepository<T extends Document> {
  create(item: T): Promise<T>;

  retrieve(): Promise<Array<T>>;

  update(_id: Types.ObjectId, item: T);

  delete(_id: string): Promise<T>;

  findById(_id: string): Promise<T>;
}
