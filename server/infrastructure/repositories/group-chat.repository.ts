import { IGroupChatRepository } from '../../domain/aggregates/user-start-group-chat.aggregate/group-chat.repository.interface';
import { GroupChatRoom } from '../../domain/aggregates/user-start-group-chat.aggregate/group-chat-room';
import { injectable, inject } from 'inversify';
import { MongoRepository } from './mongo.repository';
import { GroupChatRoomModel } from '../data-access/models/group-chat-room.model';
import { GroupChatRoomDocument } from '../data-access/documents/group-chat-room.document';
import TYPES from '../../constant/types';
import { UserSignUpDataRepository } from './user-sign-up.repository';
import { UserDocument } from '../data-access/documents/user.document';
import { MessageDocument } from '../data-access/documents/message.document';

@injectable()
export class GroupChatRepository implements IGroupChatRepository {
    constructor(
        @inject(TYPES.GroupChatDataRepository)
        private groupChatDataRepository: GroupChatDataRepository,
        @inject(TYPES.UserSignUpDataRepository)
        private userSignUpDataRepository: UserSignUpDataRepository
    ) {}
    async createGroupChat(room: GroupChatRoom): Promise<GroupChatRoom> {
        let condition = {
            NickName: room.starter.nickName,
            Email: room.starter.email
        };

        let starterDocument: UserDocument = await this.userSignUpDataRepository.getOne(
            condition
        );

        let members: Array<UserDocument> = new Array<UserDocument>();
        members.push(starterDocument);
        let groupChatDocument: GroupChatRoomDocument = {
            Starter: starterDocument,
            Messages: null,
            Members: members,
            BlackList: null
        };
        let chatRoomDocument: GroupChatRoomDocument = await this.groupChatDataRepository.create(
            groupChatDocument
        );
        let chatRoom: GroupChatRoom = new GroupChatRoom(
            chatRoomDocument.Starter.NickName,
            chatRoomDocument.Starter.Email,
            chatRoomDocument._id
        );
        if (chatRoomDocument.Members) {
            chatRoomDocument.Members.forEach(member => {
                chatRoom.addMember(member.NickName, member.Email);
            });
        }
        if (chatRoomDocument.Messages) {
            chatRoomDocument.Messages.forEach(message => {
                chatRoom.addMessage(
                    message.Sender.NickName,
                    message.Sender.Email,
                    message.Content
                );
            });
        }
        return chatRoom;
    }

    async getGroupChat(id: string): Promise<GroupChatRoom> {
        let chatRoomDocument: GroupChatRoomDocument = await this.groupChatDataRepository.findById(
            id
        );
        let chatRoom: GroupChatRoom = new GroupChatRoom(
            chatRoomDocument.Starter.NickName,
            chatRoomDocument.Starter.Email,
            chatRoomDocument._id
        );
        if (chatRoomDocument.Members) {
            chatRoomDocument.Members.forEach(member => {
                chatRoom.addMember(member.NickName, member.Email);
            });
        }
        if (chatRoomDocument.Messages) {
            chatRoomDocument.Messages.forEach(message => {
                chatRoom.addMessage(
                    message.Sender.NickName,
                    message.Sender.Email,
                    message.Content
                );
            });
        }

        return chatRoom;
    }

    async getAllGroups(): Promise<Array<GroupChatRoom>> {
        let chatRooms: Array<GroupChatRoom> = new Array<GroupChatRoom>();
        let chatRoomDocuments: Array<
            GroupChatRoomDocument
        > = await this.groupChatDataRepository.retrieve();
        chatRoomDocuments.forEach(chatRoomDocument => {
            let chatRoom: GroupChatRoom = new GroupChatRoom(
                chatRoomDocument.Starter.NickName,
                chatRoomDocument.Starter.Email,
                chatRoomDocument._id
            );
            if (chatRoomDocument.Members != null) {
                chatRoomDocument.Members.forEach(member => {
                    chatRoom.addMember(member.NickName, member.Email);
                });
            }

            if (chatRoomDocument.Messages != null) {
                chatRoomDocument.Messages.forEach(message => {
                    chatRoom.addMessage(
                        message.Sender.NickName,
                        message.Sender.Email,
                        message.Content
                    );
                });
            }
            chatRooms.push(chatRoom);
        });

        return chatRooms;
    }

    async deleteGroupChat(chatRoomId: string): Promise<GroupChatRoom> {
        return null;
    }

    async addGroupMember(
        roomId: string,
        nickName: string,
        email: string
    ): Promise<GroupChatRoom> {
        let chatroom = await this.groupChatDataRepository.findById(roomId);
        let member: UserDocument = await this.userSignUpDataRepository.getOne({
            NickName: nickName,
            Email: email
        });
        console.log(chatroom);
        chatroom.Members.push(member);
        this.groupChatDataRepository.update(chatroom._id, chatroom);
        return null;
    }

    async DeleteGroupMember(email: string): Promise<GroupChatRoom> {
        return null;
    }

    async addMessage(
        roomId: string,
        nickName: string,
        email: string,
        content: string
    ): Promise<GroupChatRoom> {
        console.log(roomId);
        let chatroom = await this.groupChatDataRepository.findById(roomId);
        let user = await this.userSignUpDataRepository.getOne({
            NickName: nickName,
            Email: email
        });
        let message: MessageDocument = {
            Content: content,
            DateTime: new Date(),
            Sender: user
        };

        if (chatroom.Messages == null) {
            let messages = new Array<MessageDocument>();
            messages.push(message)
            chatroom.Messages = messages;
        } else {
            chatroom.Messages.push(message);
        }

        console.log("chatroom.Messages");
        this.groupChatDataRepository.update(chatroom._id, chatroom);

        return null;
    }
}

@injectable()
export class GroupChatDataRepository extends MongoRepository<
    GroupChatRoomDocument
> {
    constructor() {
        super(GroupChatRoomModel);
    }

    async getOne(conditions): Promise<GroupChatRoomDocument> {
        return await GroupChatRoomModel.findOne(conditions).exec();
    }
}
