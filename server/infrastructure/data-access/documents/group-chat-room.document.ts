import { Document } from 'mongoose';
import { UserDocument } from './user.document';
import { MessageDocument } from './message.document';

export interface GroupChatRoomDocument extends Document {
    _id?: string;
    Starter: UserDocument;
    Messages: Array<MessageDocument>;
    Members: Array<UserDocument>;
    BlackList: Array<UserDocument>;
}
