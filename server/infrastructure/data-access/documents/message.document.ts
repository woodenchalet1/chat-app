import { Document } from 'mongoose';
import { UserDocument } from './user.document';

export interface MessageDocument extends Document {
    _id?: string;
    Content: string;
    DateTime: Date;
    Sender: UserDocument;
}