import { Document } from 'mongoose';

export interface UserDocument extends Document {
    _id?: string;
    NickName: string;
    Email: string;
    Password: string;
}