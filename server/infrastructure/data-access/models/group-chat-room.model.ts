import { Model, model } from 'mongoose';
import { GroupChatRoomDocument } from '../documents/group-chat-room.document';
import { GroupChatRoomSchema } from '../schemas/group-chat-room.schema';

export const GroupChatRoomModel: Model<GroupChatRoomDocument> = model<GroupChatRoomDocument>('GroupChatRoom', GroupChatRoomSchema);