import { Model, model } from 'mongoose';
import { UserDocument } from '../documents/user.document';
import { UserSchema } from '../schemas/user.schema';
import { MessageDocument } from '../documents/message.document';
import { MessageSchema } from '../schemas/message.schema';


export const MessageModel: Model<MessageDocument> = model<MessageDocument>('MessageModel', MessageSchema);