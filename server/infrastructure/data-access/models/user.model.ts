import { Model, model } from 'mongoose';
import { UserDocument } from '../documents/user.document';
import { UserSchema } from '../schemas/user.schema';


export const UserModel: Model<UserDocument> = model<UserDocument>('UserModel', UserSchema);