import { Schema } from 'mongoose';
import { UserSchema } from './user.schema';

export const MessageSchema: Schema = new Schema({
    Content: String,
    DateTime: Date,
    Sender: UserSchema
});
