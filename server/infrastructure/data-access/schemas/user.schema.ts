import { Schema } from 'mongoose';

export const UserSchema: Schema = new Schema({
    NickName: String,
    Email: String,
    Password: String,
});
