import { Schema } from 'mongoose';
import { UserSchema } from './user.schema';
import { MessageSchema } from './message.schema';

export const GroupChatRoomSchema: Schema = new Schema({
    Starter: UserSchema,
    Messages: [MessageSchema],
    Members:[UserSchema],
    BlackList:[UserSchema],
});
