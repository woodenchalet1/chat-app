import { User } from './user';
import { Message } from './message';

export class ChatRoom {
    private _chatRoomId: string;

    private _starter: User;

    private _messages: Array<Message>;

    constructor(chatRoomId: string, starter: User) {
        this._chatRoomId = chatRoomId;
        this._starter = starter;
        this._messages = new Array<Message>();
    }

    public addMessage(sender: User, content: string){
        this._messages.push(new Message(sender, content));
    }

    public get chatRoomId(): string {
        return this._chatRoomId;
    }

    public get starter(): User {
        return this._starter;
    }
}
