import { User } from './user';

export class Message {
    private _content: string;
    private _datetime: Date;
    private _sender: User;

    constructor(sender: User, content: string) {
        this._content = content;
        this._sender = sender;
        this._datetime = new Date();
    }

    public get content(): string {
        return this._content;
    }

    public get datetime(): Date {
        return this._datetime;
    }

    public get sender(): User {
        return this._sender;
    }
}
