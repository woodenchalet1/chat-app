import { ChatRoom } from './chat-room';
import { User } from './user';

export class OneOnOneChatRoom extends ChatRoom {

    private _partner: User;

    constructor(chatRoomId: string, starter: User, partner: User ) {
        super(chatRoomId, starter);
        this._partner = partner;
    }

    public get partner(): User {
        return this._partner;
    }
}
