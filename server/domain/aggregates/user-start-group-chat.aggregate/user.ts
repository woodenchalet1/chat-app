export class User {
    constructor(nickName: string, email: string) {
        this._email = email;
        this._nickName = nickName;
    }

    public get nickName(): string {
        return this._nickName;
    }

    public get email(): string {
        return this._email;
    }

    private _nickName: string;

    private _email: string;

    public toJSON(): any {
        return {
            email: this._email,
            nickName: this._nickName
        };
    }
}
