import { GroupChatRoom } from './group-chat-room';

export interface IGroupChatRepository {
    createGroupChat(room: GroupChatRoom): Promise<GroupChatRoom>;

    getGroupChat(chatRoomId: string): Promise<GroupChatRoom>;

    deleteGroupChat(chatRoomId: string): Promise<GroupChatRoom>;

    getAllGroups(): Promise<Array<GroupChatRoom>>;

    addGroupMember(
        roomId: string,
        Name: string,
        email: string
    ): Promise<GroupChatRoom>;

    DeleteGroupMember(email: string): Promise<GroupChatRoom>;

    addMessage(
        roomId: string,
        nickName: string,
        email: string,
        content: string
    ): Promise<GroupChatRoom>;
}
