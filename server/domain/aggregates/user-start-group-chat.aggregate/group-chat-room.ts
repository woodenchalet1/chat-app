import { ChatRoom } from '../user-start-group-chat.aggregate/chat-room';
import { User } from '../user-start-group-chat.aggregate/user';

export class GroupChatRoom extends ChatRoom {
    private _members: Array<User>;

    private _blackList: Array<User>;

    constructor(nickName: string, email: string, id?: string) {
        super(new User(nickName, email), id);
        this._members = new Array<User>();
        this._blackList = new Array<User>();
    }

    public addMember(nickName: string, email: string) {
        this._members.push(new User(nickName, email));
    }

    public deleteMember(email: string) {
        const index = this._members.findIndex(element => {
            return element.email == email;
        });
        if (index != -1) {
            this._blackList.concat(this._members.splice(index, 1));
        }
    }

    public get members(): Array<User> {
        return this._members;
    }

    public toJSON(): any {

        return {
            Id: this.Id,
            members: this._members,
            messages: this.messages,
            starter: this.starter,
            blackList: this._blackList,
        };
    }
}
