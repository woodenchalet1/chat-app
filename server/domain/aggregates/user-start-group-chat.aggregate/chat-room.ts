import { User } from './user';
import { Message } from './message';

export class ChatRoom {
    private _id: string;

    private _starter: User;

    private _messages: Array<Message>;

    constructor(starter: User,chatRoomId?: string, ) {
        this._starter = starter;
        this._messages = new Array<Message>();
        this._id = chatRoomId;
    }

    public addMessage(nickName: string, email: string, content: string) {
        this._messages.push(new Message(new User(nickName, email), content));
    }

    public get Id(): string {
        return this._id;
    }

    public get messages(): Array<Message> {
        return this._messages;
    }

    public get starter(): User {
        return this._starter;
    }
}
