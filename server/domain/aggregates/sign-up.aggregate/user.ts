export class User {
    /**
     *
     */
    constructor(
        id: string,
        nickName: string,
        email: string,
        password?: string
    ) {
        this._id = id;
        this._email = email;
        this._nickName = nickName;
        this._password = password;
    }

    public get nickName(): string {
        return this._nickName;
    }

    public get email(): string {
        return this._email;
    }

    public get password(): string {
        return this._password;
    }

    public get id(): string {
        return this._id;
    }

    private _id: string;

    private _nickName: string;

    private _email: string;

    private _password: string;

    public toJSON(): any {

        return {
            email: this._email,
            nickName: this._nickName,
            id: this._id
        };
    }
}
