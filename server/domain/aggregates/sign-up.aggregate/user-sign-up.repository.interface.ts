import { User } from './user';

export interface IUserSignUpRepository {
    UserSignUp(nickName: string, email: string, password: string): Promise<User>;
    getUsers(): Promise<Array<User>>;
    getUser(conditions): Promise<User>;
}
