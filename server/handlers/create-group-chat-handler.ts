import { inject, injectable } from 'inversify';
import TYPES from '../constant/types';
import { GroupChatRepository } from '../infrastructure/repositories/group-chat.repository';
import { GroupChatRoom } from '../domain/aggregates/user-start-group-chat.aggregate/group-chat-room';

@injectable()
export class CreateGroupChatHandler {
    constructor(
        @inject(TYPES.GroupChatRepository)
        private groupChatRepository: GroupChatRepository
    ) {}

    public async HandleCreateGroupChat(
        createrNickName: string,
        createrEmail: string
    ): Promise<GroupChatRoom> {
        return await this.groupChatRepository.createGroupChat(
            new GroupChatRoom(createrNickName, createrEmail)
        );
    }

    public async HandleGetGroupChat(chatRoomId: string) {
        let chatRoom: GroupChatRoom = await this.groupChatRepository.getGroupChat(
            chatRoomId
        );

        let result = [];
        if (chatRoom) {
            chatRoom.messages.forEach(message => {

                result.push({
                    sender: {
                        nickName: message.sender.nickName,
                        email: message.sender.email
                    },
                    dataTime: message.datetime,
                    content: message.content
                });
            });

            return result;
        } else {
            return [];
        }
    }

    public async HandleAddChatToGroup(
        nickName: string,
        email: string,
        content: string,
        chatRoomId: string
    ) {
        this.groupChatRepository.addMessage(
            chatRoomId,
            nickName,
            email,
            content
        );
    }

    public async HandleAddMember(
        chatRoomId: string,
        nickName: string,
        email: string
    ): Promise<GroupChatRoom> {
        return await this.groupChatRepository.addGroupMember(
            chatRoomId,
            nickName,
            email
        );
    }

    public async HandleGetGroups(): Promise<Array<GroupChatRoom>> {
        return await this.groupChatRepository.getAllGroups();
    }
}
