import { IUserSignUpRepository } from '../domain/aggregates/sign-up.aggregate/user-sign-up.repository.interface';
import { injectable, inject } from 'inversify';
import TYPES from '../constant/types';
import { sign } from 'jsonwebtoken';
import { User } from '../domain/aggregates/sign-up.aggregate/user';

@injectable()
export class UserHandler {
    constructor(
        @inject(TYPES.UserSignUpRepository)
        private signUpRepository: IUserSignUpRepository
    ) {}

    public async HandleSignUp(
        nickName: string,
        email: string,
        password: string
    ): Promise<User> {
        return await this.signUpRepository.UserSignUp(
            nickName,
            email,
            password
        );
    }

    public async HandleLogin(email: string, password: string) {
        let user: User = await this.signUpRepository.getUser({
            Email: email,
            Password: password
        });

        let token = sign({ email: user.email }, 'secret');

        return { nickName: user.nickName, email: user.email, token: token };
    }

    public async HandleGetAllUser(): Promise<Array<User>> {
        return await this.signUpRepository.getUsers();
    }
}
